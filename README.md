# spartan

Collection of scripts for Spartan-3AN FPGA.

## XILinx Utils

[`xilu`](xilu.sh) is a small script to compile files starting from a `xise` configuration and to upload `bit` files to the FPGA.

### Installation

Clone [f3fora/spartan](https://gitlab.com/f3fora/spartan) and put `xilu.sh` into a directory of `$PATH` as `xilu` to install this utility.

For example, if `$HOME/.bin` is in `$PATH`, do `ln /path/to/xilu.sh $HOME/.bin/xilu`

### Requirements

- [Xilinx ISE](https://www.xilinx.com/support/download/index.html/content/xilinx/en/downloadNav/vivado-design-tools/archive-ise.html)
- find
- awk
- [xpath](https://metacpan.org/dist/XML-XPath) (e.g. for [Ubuntu Focal](https://packages.ubuntu.com/focal/libxml-xpath-perl), [Arch Linux](https://archlinux.org/packages/community/any/perl-xml-xpath/))

### Usage

- `xilu` -> show the help message

Some basic commands:

- `xilu my_xise_file.xise` -> run `Generate Programming File`
- `xilu my_bit_file.bit` -> upload the binary to the FPGA
