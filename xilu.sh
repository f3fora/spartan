#!/usr/bin/env sh

# Description: xilu is a small script to compile files starting from a xise configuration and upload bit files to the Xilinx Spartan-3AN FPGA.
#
# Dependencies: xilinx ise tools [Xilinx_ISE_DS_Lin_14.7_1015_1.tar], find, awk, xpath [perl-xml-xpath]
#
# Shell: POSIX compliant

# source Xilinx settings command
settings() {
    # shellcheck disable=SC1091
    . /opt/Xilinx/14.7/ISE_DS/settings64.sh >/dev/null 2>&1
}

settings || {
    printf "source settings64.sh fails\n"
    exit 1
}

command -v xpath >/dev/null 2>&1 || {
    printf "xpath is not installed\n"
    exit 1
}

command -v find >/dev/null 2>&1 || {
    printf "find is not installed\n"
    exit 1
}

# constants
synthesize="Synthesize - XST"
implement="Implement Design"
generate="Generate Programming File"

# variables
task=""
compile=0
upload=0
clean=0
help=0

if [ $# -eq 0 ]; then
    help=1
fi

while [ $# -gt 0 ]; do
    case "$1" in
        -s | --synthesize)
            if [ ${compile} -lt 1 ]; then
                compile=1
                task="${synthesize}"
            fi
            ;;
        -i | --implement)
            if [ ${compile} -lt 2 ]; then
                compile=2
                task="${implement}"
            fi
            ;;
        -g | --generate)
            compile=3
            task="${generate}"
            ;;
        -c | --clean)
            clean=1
            ;;
        *.xise)
            xise="$1"
            if [ ! -f "${xise}" ]; then
                printf "%s does not exist\n" "${xise}"
                exit 1
            fi
            ;;
        -u | --upload)
            upload=1
            ;;
        *.bit)
            upload=1
            bit="$1"
            if [ ! -f "${bit}" ]; then
                printf "%s does not exist\n" "${bit}"
                exit 1
            fi
            ;;
        -h | --help | *)
            help=1
            break
            ;;
    esac
    shift
done

# show help
if [ ${help} -eq 1 ]; then
    prog=$(basename "$0")
    printf "Usage: %s [option]... [xise]" "${prog}"
    printf "\nDo an action with the [xise] file"
    printf "\n"
    printf "\nBy default, the action is Generate Programming File"
    printf "\nThe actions can be specified with [option]..."
    printf "\n"
    printf "\n\t-s, --synthesize\tSynthesize - XST"
    printf "\n\t-i, --implement\t\tImplement Design"
    printf "\n\t-g, --generate\t\tGenerate Programming File"
    printf "\n\t-c, --clean\t\tRemove all generated files"
    printf "\n\t-u, --upload\t\tUpload the generated [bit] to FPGA"
    printf "\n"
    printf "\nUsage: %s [bit]" "${prog}"
    printf "\nUpload the compiled [bit] file to FPGA"
    printf "\n"
    printf "\nThis script has been tested only with Xilinx Spartan-3AN FPGA and with ise installed via Xilinx_ISE_DS_Lin_14.7_1015_1.tar"
    printf "\nFor more information, visit https://gitlab.com/f3fora/spartan"
    printf "\n\n"
    exit 0
fi

# exit if no file is provided
if [ -z "${xise}" ] && [ -z "${bit}" ]; then
    printf "Choose a .xise or a .bit file\n"
    exit 1
fi

# exit if clean and no xise is provided
if [ -z "${xise}" ] && [ ${clean} -eq 1 ]; then
    printf "Choose a .xise file\n"
    exit 1
fi

if [ -n "${xise}" ]; then
    path=$(dirname "$(realpath "${xise}")")

    # clean project removing files
    if [ ${clean} -eq 1 ]; then
        # read the xise file and build a regex
        other_files=$(xpath -q -e '/project/files/file/@xil_pn:name' "${xise}" | awk -F'"' '{if(NR==1){s=s".*\\("$2} else {s=s"\\|"$2}}END{print s"\\)"}')
        # remove all files except xise and the ones mentioned in it
        find "${path}" -mindepth 1 -maxdepth 1 -not -name "$(basename "${xise}")" -a -not -regex "${other_files}" -exec rm -r "{}" \;
        exit 0
    fi

    # set default task if no action is passed
    if [ ${compile} -eq 0 ]; then
        compile=3
        task="Generate Programming File"
    fi

    # compile
    if [ ${compile} -gt 0 ]; then
        # open the project
        # get the status of the current project
        # if not updated, recompile it
        # if errors, exit
        settings && xtclsh <<EOF || exit 1
project open ${xise}
set status [ process get "${task}" status ]
if { ( \$status != "up_to_date" ) && ( \$status != "warnings" ) } {
    process run "${task}"
    set status [ process get "${synthesize}" status ] 
    if { ( \$status == "errors" ) || (( \$status == "never_run" ) && ( ${compile} >= 1 )) } {
        exit 1
    }
    set status [ process get "${implement}" status ] 
    if { ( \$status == "errors" ) || (( \$status == "never_run" ) && ( ${compile} >= 2 )) } {
        exit 1
    }
    set status [ process get "${generate}" status ] 
    if { ( \$status == "errors" ) || (( \$status == "never_run" )  && ( ${compile} >= 3 ))} {
        exit 1
    }
}
EOF
    fi

    # compute the bit file name for -u flag
    if [ -z "${bit}" ] && [ ${upload} -eq 1 ]; then
        # TODO:
        # There is a property which should be the output name. It's possible to obtained it with
        # xpath -q -e "/project/properties/property[@xil_pn:name=\"Output File Name\"]/@xil_pn:value" ${xise}
        # However sometime the output files are saved with a different names. IDK why
        # WORKAROUND: find the *.bit files
        bit=$(find "${path}" -name "*.bit" -exec basename "{}" \;)
        if [ -z "${bit}" ]; then
            printf "bit file not found\n"
            exit 1
        elif [ "$(echo "$bit" | wc -l)" -gt 1 ]; then
            printf "several bit files found\n"
            exit 1
        fi
    fi

fi

if [ -n "${bit}" ]; then
    # bit file path has to be an absolute one
    bit=$(realpath "${bit}")

    #upload=0
    # upload to fpga
    if [ ${upload} -eq 1 ]; then
        # do whatever is needed
        # quit! quit! quit! [crucial]
        settings && impact -batch <<EOF
setMode -bs
setCable -port auto
Identify -inferir 
identifyMPM 
assignFile -p 1 -file "${bit}"
Program -p 1 -onlyFpga 
deleteDevice -position 1
quit
EOF

        printf "\n"
        # clean impact log files
        path=$(dirname "$(realpath "${bit}")")
        find "${path}" -mindepth 1 -name "_impact*" -type f -delete
    fi
fi
